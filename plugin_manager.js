/***

	Plugin Manager v:0.1
	Based on DiscordBot by Chalda (https://github.com/chalda/DiscordBot)
  Rewritten by Shufflertoxin

***/

var fs = require('fs'),
    path = require('path');

/**
	Returns the directory data for Plugins.
**/
function getDirectories(srcpath) {
    return fs.readdirSync(srcpath).filter(function(file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
}

var plugin_folders;
var plugin_directory;

plugin_directory = "./plugins/";
plugin_folders = getDirectories(plugin_directory);

/**
	Init function for Plugin Manager
**/
exports.init = function(){
    preload_plugins();
};

/**
	Returns a array of all Dependencies (for NPM to install)
**/
function createNpmDependenciesArray (packageFilePath) {
    var p = require(packageFilePath);
    if (!p.dependencies) return [];
    var depends = [];
    for (var mod in p.dependencies) {
        depends.push(mod + "@" + p.dependencies[mod]);
    }

    return depends;
}

/**
	Main function.
**/
function preload_plugins(){
    var depends = [];
    var npm = require("npm");
    for (var i = 0; i < plugin_folders.length; i++) {
        try{
            require(plugin_directory + plugin_folders[i]);
        } catch(e) {
            depends = depends.concat(createNpmDependenciesArray(plugin_directory + plugin_folders[i] + "/package.json"));
        }
    }
    if(depends.length > 0) {
		// Load NPM  dependencies.
        npm.load({
            loaded: false
        }, function (err) {
            npm.commands.install(depends, function (er, data) {
                if(er){
                    console.log(er);
                }
                console.log("Plugin preload complete");
                load_plugins()
            });

            if (err) {
                console.log("preload_plugins: " + err);
            }
        });
    } else {
        load_plugins()
    }
}

/**
	Plugin Loader
**/
function load_plugins(){
    var helperbot = require("./helperbot.js");
    var commandCount = 0;
    for (var i = 0; i < plugin_folders.length; i++) {
        var plugin;
        try{
            plugin = require(plugin_directory + plugin_folders[i])
        } catch (err){
            console.log("Improper setup of the '" + plugin_folders[i] +"' plugin. : " + err);
        }
        if (plugin){
            if("commands" in plugin){
                for (var j = 0; j < plugin.commands.length; j++) {
                    if (plugin.commands[j] in plugin){
                        helperbot.addCommand(plugin.commands[j], plugin[plugin.commands[j]])
                        commandCount++;
                    }
                }
            }
        }
    }
	//console.log("" + plugin_folders[1])
    console.log("Loaded " + helperbot.commandCount() + " chat commands type !help in Discord for a commands list.")
}
