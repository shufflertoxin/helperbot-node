/***

	Helerbot v:0.1
  Written by Shufflertoxin

***/

var commands = {}

const Discord = require('discord.js')
var helperbot = new Discord.Client();
helperbot.loginWithToken('token', output);
function output(error, token) {
	if (error) {
		console.log('There was an error logging in: ' + error);
		return;
	} else
		console.log('Logged in. Token: ' + token);
}

helperbot.on("ready", function () {
	console.log("Ready to begin! Serving in " + helperbot.channels.length + " channels");
	require("./plugin_manager.js").init();
});


helperbot.on('message', function(message) {
	if(message.author.id != helperbot.user.id && (message.content[0] === '!' || message.content.indexOf(helperbot.user.mention()) == 0)){
		var commandText = message.content.split(" ")[0].substring(1);
		var args = commandText.substring(commandText.length+2);
		var command = commands[commandText];
		if (command) {
			try {
				command.process(helperbot, message, args);
			} catch(e) {
				helperbot.sendMessage(message.channel, "command " + commandText + " failed :(\n" + e.stack);
			}
		} else {
			helperbot.sendMessage(message.channel, "Invalid command " + commandText);
		}
	}

});

exports.commandCount = function(){
    return Object.keys(commands).length;
}

exports.addCommand = function(commandName, commandObject){
    try {
        commands[commandName] = commandObject;
    } catch(err){
        console.log(err);
    }
}